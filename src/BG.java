import java.awt.Color;
import java.awt.Graphics2D;

public class BG {
Color color;
int h,w;

public BG() {
	color = Color.BLUE;
	h = GamePanel.HEIGHT;
	w = GamePanel.WIDTH;
}

public void draw(Graphics2D g) {
	g.setColor(color);
	g.fillRect(0, 0, w, h);
}
}
