import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;

public class Block {
private int x,y,s, number,timer = -1,time, colN,rowN;
private Color color;
private int[] cyan = {0, 183, 235};
private int alpha = 255;
private boolean fadin = false;
public Block(int x, int y, int s, int number, int rowN, int colN) {
	this.x = x;
	this.y = y;
	this.s = s;
	this.number = number;
	color = Color.CYAN;
	this.colN = colN;
	this.rowN = rowN;
}
public int getX() {
	return x;
}
public int getY() {
	return y;
}
public int getN() {
	return number;
}
public int getCol() {
	return colN;
}
public int getAlpha() {
	return alpha;
}
public boolean getFadin() {
	return fadin;
}
public void setFadin(boolean b) {
	fadin = b;
}
public void fade(int timer, int time) {
	this.timer = timer;
	this.time = time;
	fadin = true;
}
public void draw(Graphics2D g) {
	if (timer > 0) {
		timer--;
	}
		
	if (timer == 0) {
		alpha-=255/time;
		if (alpha < 0) {
			alpha = 0;
		}
	}
	
	g.setColor(new Color(cyan[0], cyan[1], cyan[2], alpha));
	g.setStroke(new BasicStroke(3));
	g.fillRect(x, y, s, s);
	g.setColor(Color.BLACK);
	g.drawRect(x, y, s, s);
}
}
