import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;

public class Element {
int s, w, h;
Color color;
ArrayList<Integer> coords;
public Element(int x, int y, int s) {
	color = Color.CYAN;
	this.s = s;
	coords = new ArrayList<Integer>();
	int ran = (int) (Math.random()*6);
	//ran = 3;
	if (ran == 0) {
		coords.add(x);
		coords.add(y);
	}
	if (ran == 1) {
		coords.add(x-s/2);
		coords.add(y);
		coords.add(x+s/2);
		coords.add(y);
	}
	if (ran == 2) {
		coords.add(x);
		coords.add(y-s/2);
		coords.add(x);
		coords.add(y+s/2);
	}
	if (ran == 3) {
		coords.add(x);
		coords.add(y-s/2);
		coords.add(x);
		coords.add(y+s/2);
		coords.add(x-s);
		coords.add(y+s/2);
		coords.add(x+s);
		coords.add(y+s/2);
	}
	if (ran == 4) {
		coords.add(x-s-s/2);
		coords.add(y);
		coords.add(x-s/2);
		coords.add(y);
		coords.add(x+s/2);
		coords.add(y);
		coords.add(x+s+s/2);
		coords.add(y);
	}
	if (ran == 5) {
		coords.add(x-s);
		coords.add(y-s);
		coords.add(x-s);
		coords.add(y);
		coords.add(x-s);
		coords.add(y+s);
		coords.add(x);
		coords.add(y-s);
		coords.add(x);
		coords.add(y);
		coords.add(x);
		coords.add(y+s);
		coords.add(x+s);
		coords.add(y-s);
		coords.add(x+s);
		coords.add(y);
		coords.add(x+s);
		coords.add(y+s);
	}
}
public void draw(Graphics2D g) {
	for (int i = 0; i < coords.size(); i+=2) {
	g.setColor(color);
	g.fillRect(coords.get(i), coords.get(i+1), s, s);
	g.setStroke(new BasicStroke(3));
	g.setColor(Color.BLACK);
	g.drawRect(coords.get(i), coords.get(i+1), s, s);
	}
}
}
