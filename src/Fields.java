import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;

public class Fields {
	private boolean free;
int s;
Color color;
int x,y;
public Fields(int x, int y, int s) {
	color = Color.GRAY;
	this.x = x;
	this.y = y;
	this.s = s;
	free = true;
}
public int getX() {
	return x;
}
public int getY() {
	return y;
}
public int getS() {
	return s;
}
public boolean getF() {
	return free;
}
public void setF(boolean b) {
	free = b;
}
public void draw(Graphics2D g) {
	g.setColor(color);
	g.setStroke(new BasicStroke(3));
	g.fillRect(x,y,s,s);
	g.setColor(Color.BLACK);
	g.drawRect(x, y, s, s);
}
}
