import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.JPanel;

public class GamePanel extends JPanel implements Runnable{
	
	public static int WIDTH = 900;
	public static int HEIGHT = 900;
	
private BufferedImage image;
	
	private Graphics2D g;
	
	private int FPS = 30;
	private double millisToFPS = 1000/FPS;
	private long timerFPS;
	private int sleepTime;
	
	BG bg;
	public static ArrayList<ArrayList<Fields>> fields;
	Menu menu;
	public static ArrayList<Element> elements;
	public static ArrayList<ArrayList<Block>> blocks;
	public static ArrayList<ArrayList<Integer>> bloccs;
	
	public GamePanel() {
		super();
		
		setPreferredSize(new Dimension(WIDTH, HEIGHT));
		setFocusable(true);
		requestFocus();
		
		addMouseMotionListener(new Listeners());
		addMouseListener(new Listeners());
		
		
	elements = new ArrayList<Element>();
	blocks = new ArrayList<ArrayList<Block>>();
	bloccs = new ArrayList<ArrayList<Integer>>();
	bg = new BG();
	menu = new Menu(60, 675, 780,225);
	fields = new ArrayList<ArrayList<Fields>>();
	int h = 50;
	int w = 150;
	int s = 75;
	for (int i = 0; i < 8; i++) {
		fields.add(new ArrayList<Fields>());
		blocks.add(new ArrayList<Block>());
		bloccs.add(new ArrayList<Integer>());
		for (int a = 0; a < 8; a++) {
		fields.get(i).add(new Fields(w+s*i,h+s*a, s));
		}
	}
	}
	
	public void start() {
		Thread t = new Thread(this);
		t.start();
	}
	
	@Override
	public void run() {
		image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
		g = (Graphics2D) image.getGraphics();
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		while (true) {
			timerFPS = System.nanoTime();
			
			gameUpdate();
			gameRender();
			gameDraw();
			
			
			timerFPS = (System.nanoTime() - timerFPS)/1000000;
			if (timerFPS < millisToFPS) {
				sleepTime = (int) (millisToFPS - timerFPS);
			}
			else {
				sleepTime = 1;
			}
			try {
				Thread.sleep(sleepTime);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	int w1;
	private void gameUpdate() {
		ArrayList<Integer> row = new ArrayList<Integer>();
		
		for (int i = 0; i < 8; i++) {
			row.add(i);
		}
		
		for (int i = 0; i < fields.size(); i++) {
			//if (row.size() > 0) {
			//for (int a = 0; a < fields.get(i).size(); a++) {
				//if (fields.get(i).get(a).getF() && row.indexOf(a) != -1) {
					//row.remove(row.indexOf(a));
				//}
			//}
			//}
			for (int a = 0; a < row.size(); a++) {
				if (fields.get(i).get(row.get(a)).getF()) {
					row.remove(a);
					a--;
				}
			}
		}
		
		for (int i = 0; i < row.size(); i++) {
			for (int a = 0; a < blocks.size(); a++) {
				for (int b = 0; b < blocks.get(a).size(); b++) {
					if (blocks.get(a).get(b).getCol() == row.get(i) && !blocks.get(a).get(b).getFadin()) {
						blocks.get(a).get(b).fade(a*8, 30);
						fields.get(a).get(row.get(i)).setF(true);
					}
				}
			}
		}
		
		for (int i = 0; i < blocks.size(); i++) {
			if (blocks.get(i).size() == 8) {
				for (int a = 0; a < blocks.get(i).size(); a++) {
					if (!blocks.get(i).get(a).getFadin()) {
					blocks.get(i).get(a).fade(a*8, 30);
					fields.get(i).get(a).setF(true);
				}
			}
		}
		}
		
		for (int i = 0; i < blocks.size(); i++) {
			for (int a = 0; a < blocks.get(i).size(); a++) {
				if (blocks.get(i).get(a).getAlpha() == 0) {
					fields.get(i).get(blocks.get(i).get(a).getCol()).setF(true);
					blocks.get(i).remove(a);
				}
			}
		}
		
		
		
		if (elements.size() == 0) {
			 w1 = menu.spotX;
			for (int i = 0; i < 3; i++) {
				elements.add(new Element(w1, menu.spotY, 75));
				w1+=780/3;
			}
		}
		
		
		/////losing
		boolean lost = true;
		int count = 0;
		for (int i = 0; i < fields.size(); i++) {
			for (int a = 0; a < fields.size(); a++) {
				if (fields.get(i).get(a).getF()) {
					count=1;
					for (int b = 0; b < elements.size(); b++) {
					for (int c = 2; c < elements.get(b).coords.size(); c+=2) {
						int x = (int) Math.ceil((elements.get(b).coords.get(c) - elements.get(b).coords.get(0))/75.0);
						int y = (int) Math.ceil((elements.get(b).coords.get(c+1) - elements.get(b).coords.get(1))/75.0);
						if (i+x >= 0 && i+x < 8 && a+y >= 0 && a+y < 8) {
							if (fields.get(i+x).get(a+y).getF()) {
							count++;
							//System.out.println(x + " " + y + "   " + c + "   " + b);
							//System.out.println(elements.get(b).coords.get(c) - elements.get(b).coords.get(0));
							//System.out.println(elements.get(b).coords.get(c+1) - elements.get(b).coords.get(1));
							}
							else {
								break;
							}
						}
						else {
							break;
						}
					}
					if (count == elements.get(b).coords.size()/2) {
						lost = false;
						break;
					}
					count = 1;
				}
				}
				if (!lost) {
					break;
				}
			}
			if (!lost) {
				break;
			}
		}
		
		if (lost) {
			elements = new ArrayList<Element>();
			blocks = new ArrayList<ArrayList<Block>>();
			for (int i = 0; i < fields.size(); i++) {
				blocks.add(new ArrayList<Block>());
				for (int a = 0; a < fields.get(i).size(); a++) {
					fields.get(i).get(a).setF(true);
				}
			}
		}
	}
	
	private void gameRender() {
		bg.draw(g);
		for (int i = 0; i < fields.size(); i++) {
			for (int a = 0; a < fields.size(); a++) {
			fields.get(i).get(a).draw(g);
			}
		}
		menu.draw(g);
		for (int i = 0; i < blocks.size(); i++) {
			for (int a = 0; a < blocks.get(i).size();a++) {
			blocks.get(i).get(a).draw(g);
			}
		}
		for (int i = 0; i < elements.size(); i++) {
			elements.get(i).draw(g);
		}
	}
	
	private void gameDraw() {
		Graphics g2 = this.getGraphics();
		g2.drawImage(image, 0, 0, null);
		g2.dispose();
	}
}
