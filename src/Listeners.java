import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

public class Listeners implements  MouseListener, MouseMotionListener {
	public static boolean clicked = false;
	public static int x, y, x1, y1;
	public static int b;
	public static ArrayList<Integer> arr = new ArrayList<Integer>();

	@Override
	public void mouseDragged(MouseEvent e) {
		if (clicked && b != -1) {
			x = e.getX();
			y = e.getY();
			
			x1 = x-x1;
			y1 = y-y1;
			
			for (int i = 0; i < GamePanel.elements.get(b).coords.size(); i+=2) {
				GamePanel.elements.get(b).coords.set(i, GamePanel.elements.get(b).coords.get(i)+x1);
				GamePanel.elements.get(b).coords.set(i+1, GamePanel.elements.get(b).coords.get(i+1)+y1);
			}
			
			x1 = x;
			y1 = y;
			
		}
			
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		x1 = e.getX();
		y1 = e.getY();
		for (int i = 0; i < GamePanel.elements.size(); i++) {
			for (int a = 0; a < GamePanel.elements.get(i).coords.size(); a+=2) {
				if (x1 >= GamePanel.elements.get(i).coords.get(a) && 
						x1 <= GamePanel.elements.get(i).coords.get(a)+75 &&
						y1 >= GamePanel.elements.get(i).coords.get(a+1) &&
						y1 <= GamePanel.elements.get(i).coords.get(a+1)+75) {
					b = i;
					clicked = true;
					for (int c = 0; c < GamePanel.elements.get(b).coords.size(); c++) {
						arr.add(GamePanel.elements.get(b).coords.get(c));
					}
				}
			}
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if (clicked && b != -1) {
			boolean passed = false;
			ArrayList<Integer> f = new ArrayList<Integer>();
			for (int i = 0; i < GamePanel.elements.get(b).coords.size(); i+= 2) {
			int s = GamePanel.elements.get(b).coords.get(i) + 75/2;
			int sa = GamePanel.elements.get(b).coords.get(i+1) + 75/2;
			for (int a = 0; a < GamePanel.fields.size(); a++) {
				for (int j = 0; j < GamePanel.fields.size(); j++) {
				int s1 = GamePanel.fields.get(a).get(j).getX() + 75/2;
				int sa1 = GamePanel.fields.get(a).get(j).getY() + 75/2;
				if (Math.abs((s-s1)) < 75/2 && Math.abs(sa-sa1) < 75/2 && GamePanel.fields.get(a).get(j).getF()) {
					f.add(a);
					f.add(j);
					GamePanel.elements.get(b).coords.set(i, GamePanel.fields.get(a).get(j).x);
					GamePanel.elements.get(b).coords.set(i+1, GamePanel.fields.get(a).get(j).y);
				}
			}
			}
			}
			if (f.size() == GamePanel.elements.get(b).coords.size()) {
				passed = true;
			}
			
			if (!passed) {
			clicked = false;
for (int i = 0; i < arr.size(); i+=2) {
	GamePanel.elements.get(b).coords.set(i, arr.get(i));
	GamePanel.elements.get(b).coords.set(i+1, arr.get(i+1));
}
			}
			else {
				for (int i = 0; i < f.size(); i+=2) {
					GamePanel.fields.get(f.get(i)).get(f.get(i+1)).setF(false);
					//GamePanel.blocks.add(new Block(GamePanel.elements.get(b).coords.get(i),GamePanel.elements.get(b).coords.get(i+1),75,f.get(i), f.get(i), f.get(i+1)));
					if (f.get(i+1) == 7 || GamePanel.blocks.get(f.get(i)).size() == 0) {
						GamePanel.blocks.get(f.get(i)).add(new Block(GamePanel.elements.get(b).coords.get(i),GamePanel.elements.get(b).coords.get(i+1),75,f.get(i), f.get(i), f.get(i+1)));
						GamePanel.bloccs.get(f.get(i)).add(f.get(i+1));
					}
					else {
					
						int g = 0;
						while (GamePanel.blocks.get(f.get(i)).get(g).getCol() < f.get(i+1)) {
							g++;
							if (g == GamePanel.blocks.get(f.get(i)).size()) {
								break;
							}
						}
					GamePanel.blocks.get(f.get(i)).add(g, new Block(GamePanel.elements.get(b).coords.get(i),GamePanel.elements.get(b).coords.get(i+1),75,f.get(i), f.get(i), f.get(i+1)));
					GamePanel.bloccs.get(f.get(i)).add(g,f.get(i+1));
					}
					
				}
				
					GamePanel.elements.remove(b);
				
			}
			arr = new ArrayList<Integer>();
		}
		b = -1;
	}

}
